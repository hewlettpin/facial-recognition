import QtQuick 2.6
import QtQuick.Window 2.2
import QtQuick.Controls 1.5

import QtQuick.Dialogs 1.2
import QtQuick.Controls.Styles 1.2

ApplicationWindow{

    id: window1
    visible: true
    color: "#f9faff"
    width: 1366
    height: 768
    opacity: 1
    title: qsTr("Udom Facial Identity")

     signal textUpdated(string text)

    Dialog{
        id: simpleDialog
        title:  "Success Message"
        Text {
            text: qsTr("Registration Successfuly")
        }
        onAccepted: this.close()
    }

      menuBar: MenuBar {
        Menu {
            title: qsTr("File")
            MenuItem {
                id: register
                text: qsTr("&Register")
                onTriggered: console.log("Open action triggered");
            }
            MenuItem {
                text: qsTr("Exit")
                onTriggered: Qt.quit();
            }

        }

        Menu {
            title: qsTr("Report")
            MenuItem {

                text: qsTr("&New...")
                onTriggered: console.log("Open action triggered");
            }
            MenuSeparator { }
            MenuItem {
                text: qsTr("Past Days")
                onTriggered: console.log("Open data za mwanzo");

            }
        }
    }

    Item {
        x:1100
        y: 10

        Timer {
        interval: 500; running: true; repeat: true
        onTriggered: time.text = Date().toString()
        }

        Text { id: time
        font.family: "Chaparral Pro Light"
        font.pointSize:9
        }
    }

    Button {
            id: button5
            objectName: "regButton"
            x: 250
            y: 3
            width: 120
            height: 30
            text: "Register"

            style: ButtonStyle
            {
                background: Rectangle
                {
                    border.color: "steelblue"
                    color: "#4285f6"
                    antialiasing: true
                    smooth: true
                    border.width: 0
                    radius: 6
                }


                label: Text
                {
                    color: "#ffffff"
                    font.pointSize: 14
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    font.family: "Orator Std"
                    text: control.text
                }


           }

     }


    GroupBox {
        id: groupBox2
        x: 27
        y: 37
        width: 550
        height: 692
        title: qsTr("")

        Button {
            id: button3
            objectName: "importButton"
            x: 313
            y: 572
            width: 85
            height: 33
            text: "Import"
            signal ueSignalButtonClicked
            onClicked: fileDialog.open()

            style: ButtonStyle
            {
                background: Rectangle
                {
                    border.color: "steelblue"
                    color: "#4285f6"
                    antialiasing: true
                    smooth: true
                    border.width: 0
                    radius: 6
                }

                label: Text
                {
                    color: "#ffffff"
                    font.pointSize: 12
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    font.family: "lucida Bright"
                    text: control.text
                }
            }
        }

        ProgressBar {
            id: progressBar1
            objectName: "progressBar1"
            x: 20
            y: 474
            width: 498
            height: 21
             value: 0
             indeterminate: false
        }

        Button {
            id: button1
            signal callUisearch

            objectName: "cameraButton"
            x: 27
            y: 572
            width: 85
            height: 33
            text: qsTr("Camera")
            signal ueSignalButtonClicked
            onClicked: callUisearch()

            style: ButtonStyle
            {
                background: Rectangle
                {
                    border.color: "steelblue"
                    color: "#4285f6"
                    antialiasing: true
                    smooth: true
                    border.width: 0
                    radius: 6
                }
                label: Text
                {
                    color: "#ffffff"
                    font.pointSize: 12
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    font.family: "lucida Bright"
                    text: control.text
                }
            }
        }

        Button {
            id: button4
            objectName: "searchButton"
            x: 441
            y: 572
            width: 85
            height: 33
            //onClicked: simpleDialog.open()
            text: "Search"

            style: ButtonStyle
                {
                    background: Rectangle
                    {
                        border.color: "steelblue"
                        color: "#4285f6"
                        antialiasing: true
                        smooth: true
                        border.width: 0
                        radius: 6

                    }
                    label: Text
                     {
                        color: "#ffffff"
                        font.pointSize: 12
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignHCenter
                        font.family: "lucida Bright"
                        text: control.text
                    }
                }
        }

        GroupBox {
            id: groupBox3
            x: 42
            y: 36
            width: 454
            height: 371
            title: qsTr("Image For Processing")

            Image {
                id: image1
                objectName: "image1"
                x: 45
                y: 38
                width: 350
                height: 261
                smooth: true
                source: "img/imagesPhoto.png"
            }
        }

        Button {
            id: button2
            objectName: "photoButton"
            x: 145
            y: 572
            width: 85
            height: 33
            signal ueSignalButtonClicked

            text: qsTr("Photo")

            style: ButtonStyle
            {
                background: Rectangle
                {
                    border.color: "steelblue"
                    color: "#4285f6"
                    antialiasing: true
                    smooth: true
                    border.width: 0
                    radius: 6
                }

                label: Text
                {
                    color: "#ffffff"
                    font.pointSize: 12
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    font.family: "lucida Bright"
                    text: control.text
                }

            } //Button Style
        }

         Label {
            id: label17
            objectName: "label1"
            font.family: "Chaparral Pro Light"
            font.pointSize: 12
            text: ""
            x: 20
            y: 444
            width: 498
            height: 26
            verticalAlignment: Text.AlignVCenter
        }
    }

    GroupBox {
        id: groupBox1
        x: 688
        y: 37
        width: 654
        height: 692
        title: qsTr("")


        GroupBox {
            id: groupBox4
            x: 139
            y: 22
            width: 360
            height: 300
            title: qsTr("Image Result")
        }

         Label {
                    id: status
                    objectName: "status"
                    font.family: "courier New"
                    font.pointSize: 16
                    color: "#fc050b"

                    text: ""
                    x: 245
                    y: 3
                    width: 498
                    height: 26
                    font.bold: true
                    verticalAlignment: Text.AlignVCenter
                }

        Label {
            id: label1
            font.pointSize: 15
            font.family: "Century"
            x: 42
            y: 364
            width: 101
            height: 22
            text: qsTr("Full Name")
            wrapMode: Text.NoWrap
            horizontalAlignment: Text.AlignLeft
            verticalAlignment: Text.AlignVCenter
        }


        Label {
            id: label2
            objectName: "age"
            font.pointSize: 15
            font.family: "Century"
            x: 42
            y: 409
            width: 41
            height: 17
            text: qsTr("Age")
        }


        Label {
            id: label3
            font.pointSize: 15
            font.family: "Century"
            x: 42
            y: 444
            width: 18
            height: 12
            text: qsTr("Sex")
        }


        Label {
            id: label4
            font.pointSize: 15

            font.family: "Century"
            x: 42
            y: 480
            width: 48
            height: 16
            text: qsTr("Birth Date")
        }


        Label {
            id: label5
            font.pointSize: 15
            font.family: "Century"
            x: 42
            y: 513
            text: qsTr("Place of Birth")
        }


        Label {
            id: label6
            font.pointSize: 15
            font.family: "Century"
            x: 42
            y: 549
            text: qsTr("Nationality")
        }


        Label {
            id: label7
            font.pointSize: 15
            font.family: "Century"
            x: 42
            y: 583
            text: qsTr("Current Address")
        }



        Label {
            id: label8
            font.family: "Century"
            font.pointSize: 15
            x: 42
            y: 618
            text: qsTr("Number of Crimes")
        }

        Label {
            id: label9
            objectName: "fullName1"
            font.family: "Orator Std"
            x: 255
            y: 364
            width: 383
            height: 22
            verticalAlignment: Text.AlignVCenter
            font.pointSize: 14
        }

        Label {
            id: label10
            objectName: "age1"
             font.family: "Orator Std"
            x: 255
            y: 409
            width: 383
            height: 22
            verticalAlignment: Text.AlignVCenter
            font.pointSize: 14
        }

        Label {
            id: label11
            objectName: "sex1"
            font.family: "Orator Std"
            x: 255
            y: 444
            width: 383
            height: 22
            verticalAlignment: Text.AlignVCenter
            font.pointSize: 14
        }

        Label {
            id: label12
            objectName: "bDate1"
            font.family: "Orator Std"
            x: 255
            y: 480
            width: 383
            height: 22
            verticalAlignment: Text.AlignVCenter
            font.pointSize: 14
        }

        Label {
            id: label13
            objectName: "pBirth1"
            font.family: "Orator Std"
            x: 255
            y: 513
            width: 383
            height: 22
            verticalAlignment: Text.AlignVCenter
            font.pointSize: 14
        }

        Label {
            id: label14
            objectName: "nation1"
            font.family: "Orator Std"
            x: 255
            y: 549
            width: 383
            height: 22
            verticalAlignment: Text.AlignVCenter
            font.pointSize: 14
        }

        Label {
            id: label15
            objectName: "cAddress1"
            font.family: "Orator Std"
            x: 255
            y: 583
            width: 383
            height: 22
            verticalAlignment: Text.AlignVCenter
            font.pointSize: 14
        }

        Label {
            id: label16
            objectName: "crime1"
            font.family: "Orator Std"
            x: 255
            y: 618
            width: 383
            height: 22
            verticalAlignment: Text.AlignVCenter
            font.pointSize: 14
        }

        Image {
            id: image2
            objectName: "image2"
            x: 144
            y: 50
            width: 350
            height: 261
            source: "img/imagesPhoto.png"
        }




    }

     FileDialog{
        id: fileDialog
        objectName: "fileDialog"
        selectMultiple: false
        selectFolder: false
        nameFilters: "*.jpg , *.png, *.PNG, *.jpeg"
        onAccepted:{
            selectMultiple ?
            console.log(this.fileUrls)
                      :
            //label17.text = this.fileUrl
            //image1.source = this.fileUrl
            textUpdated(this.fileUrl)
        }

    }


}
