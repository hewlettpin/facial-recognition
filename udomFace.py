import sys
import sqlite3
from PyQt5.QtCore import QObject, QUrl, Qt
from PyQt5.QtWidgets import QApplication
from PyQt5.QtQml import QQmlApplicationEngine
import io
from base64 import b64encode

import numpy as np
import cv2
import face_recognition


def messageRequired():
    print("hie there")




# class Engine(QQmlApplicationEngine):
#     counter = 0
#     def __init__(self, qmls, parent=None):
#         QQmlApplicationEngine.__init__(self, parent)
#
#         [self.load("{}.qml".format(qml)) for qml in qmls]
#
#         for i, root in enumerate(self.rootObjects()):
#             if root != self.rootObjects()[0]:
#                 root.close()
#             if root != self.rootObjects()[-1]:
#                 button= root.findChild(QObject, qmls[i])
#                 button.clicked.connect(self.closeAndOpen)
#                 # button2 = root.findChild(QObject, qmls[i])
#                 # button2.clicked.connect(self.cameraPhoto)
#
#     def closeAndOpen(self):
#         self.rootObjects()[self.counter].close()
#         self.counter += 1
#         self.rootObjects()[self.counter].show()
#
#
# if __name__ == "__main__":
#         app = QApplication(sys.argv)
#         engine = Engine(['startPage', 'mat'])
#         sys.exit(app.exec_())

if __name__ == "__main__":
    app = QApplication(sys.argv)
    engine = QQmlApplicationEngine()
    ctx = engine.rootContext()
    ctx.setContextProperty("main", engine)

    engine.load('registerPage.qml')

    win = engine.rootObjects()[0]


    def enterData():
        conn = sqlite3.connect("Database/face.db", detect_types=sqlite3.PARSE_DECLTYPES)
        try:
            jinaLakwanza = win.findChild(QObject, "fname")
            fname = jinaLakwanza.property("text")
            print(fname)
            jinalapili = win.findChild(QObject, "sname")
            sname = jinalapili.property("text")
            print(sname)
            jinalamwisho = win.findChild(QObject, "lname")
            lname = jinalamwisho.property("text")
            print(lname)
            gender = win.findChild(QObject, "sex")
            jinsia = gender.property("currentIndex")
            if jinsia == 0:
                sex = "male"
            elif jinsia == 1:
                sex = "female"
            print(sex)
            dob = win.findChild(QObject, "dob")
            tarehe = dob.property("text")
            print(tarehe)
            location = win.findChild(QObject, "location")
            mahali = location.property("text")
            print(mahali)
            placeofbirth = win.findChild(QObject, "placeofbirth")
            mahariulipozaliwa = placeofbirth.property("text")
            print(mahariulipozaliwa)
            nationality = win.findChild(QObject, "nationality")
            ulaia = nationality.property("text")
            print(ulaia)
            phone = win.findChild(QObject, "phone")
            nambayasimu = phone.property("text")
            print(nambayasimu)
            crimes = win.findChild(QObject, "crimes")
            nocrimes = crimes.property("text")
            print(nocrimes)
            image = win .findChild(QObject, "imgParth")
            picha = image.property("text")
            print(picha)
            image_to_encode = face_recognition.load_image_file(picha[8:])
            image_encoding = face_recognition.face_encodings(image_to_encode)[0]
            print(image_encoding)
            with open(picha[8:], "rb") as f:
                data = f.read()
                encodedPic = b64encode(data)
                print(encodedPic)

            cursor = conn.cursor()

            conn.execute(
                "INSERT INTO suspectsTable(fname,sname,lname,dob,sex,location,placeofbirth,nationality,phone,crimes,image_encoding,image) VALUES(?,?,?,?,?,?,?,?,?,?,?,?);",
                (fname, sname, lname, tarehe, sex, mahali, mahariulipozaliwa, ulaia, nambayasimu, nocrimes, image_encoding, encodedPic))
            conn.commit()
            print("Data entered")
        except sqlite3.OperationalError:
            print("data not intered")

        conn.close()
        print("connection closed")


    def newFunc():
        engine.load('mat.qml')

        win = engine.rootObjects()[0]
        win.show()
    def cameraPage():

        button_new = win.findChild(QObject, "newPage")
        button_new.callPage.connect(newFunc())

    def loginFunction():
        db_conn = sqlite3.connect("face.db")

        thecursor = db_conn.cursor()

        username = win.findChild(QObject, "uname")
        uname = username.property("text")
        password = win.findChild(QObject, "password1")
        password1 = password.property("text")

        try:

            result =thecursor.execute((" SELECT * FROM users WHERE jina=? AND nywila=?"),(uname,password1))

            for row in result:
               if uname in row:
                    if password1 in row:
                        newFunc()

                    else:
                        print("Password incorect")
               else:
                   print("hamna jina hilo")
        except sqlite3.OperationalError:
            print("data hamna")


        db_conn.close()


    def viewData():
        db_co = sqlite3.connect("face.db")

        theCursor = db_co.cursor()

        try:
            result = theCursor.execute("SELECT susp_id, fname,sname, lname FROM suspectsTable WHERE susp_id=2")

            # You receive a list of lists that hold the result
            print(theCursor.fetchone()[0])

        except sqlite3.OperationalError:
            print("The Table Doesn't Exist")

        except:
            print("Couldn't Retrieve Data From Database")

        print("Data are")
        viewData()

        db_co.close()

    def majina():
        jinaLakwanza = win.findChild(QObject, "fname")
        fname = jinaLakwanza.property("text")
        print(fname)


    def cancelFunc():
        print("You select to Cancel")

# loginButton = win.findChild(QObject, "loginBtn")
# loginButton.clicked.connect(loginFunction)
#



addButton = win.findChild(QObject, "btnAdd")
addButton.clicked.connect(enterData)


win.show()
sys.exit(app.exec_())
