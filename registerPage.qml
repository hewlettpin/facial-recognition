import QtQuick 2.6
import QtQuick.Controls 1.5
import QtQuick.Dialogs 1.2

ApplicationWindow {
    x: 400
    y: 45
    visible: true
    width: 550
    height: 700
    color: "#f9faff"
    title: qsTr("Face login page")
    maximumWidth: 550
    minimumWidth: 550
    maximumHeight: 700
    minimumHeight: 700


    Label {
        id: label1
        x: 117
        y: 29
        width: 310
        height: 38
        color: "#306586"
        text: qsTr("REGISTRATION FORM")
        horizontalAlignment: Text.AlignHCenter
        font.pointSize: 22
    }

    GroupBox {
        id: groupBox1
        x: 59
        y: 106
        width: 432
        height: 158
        title: qsTr("Personal Details")

        TextField {
            id: textField1
            objectName: "fname"
            x: 3
            y: 53
            width: 127
            height: 26
            placeholderText: qsTr("Enter first name")

        }

        TextField {
            id: textField2
             objectName: "sname"
            x: 145
            y: 53
            width: 127
            height: 26
            placeholderText: qsTr("Enter second name")
        }

        TextField {
            id: textField3
             objectName: "lname"
            x: 289
            y: 53
            width: 127
            height: 26
            placeholderText: qsTr("Enter surname")
        }

        Label {
            id: label2
            x: 3
            y: 23
            width: 95
            height: 18
            text: qsTr("First Name :")
            font.pointSize: 10
        }

        Label {
            id: label3
            x: 145
            y: 24
            width: 112
            height: 18
            text: qsTr("Second Name :")
            font.pointSize: 10
        }

        Label {
            id: label4
            x: 289
            y: 23
            width: 95
            height: 18
            text: qsTr("Surname :")
            font.pointSize: 10
        }

        Label {
            id: label5
            x: 3
            y: 99
            width: 62
            height: 17
            text: qsTr("Gender :")
            font.pointSize: 10
        }

        ComboBox {
            id: comboBox1
            textRole: "key"
             objectName: "sex"
            x: 80
            y: 94
            width: 89
            height: 27
            model: ListModel{
                ListElement {key: "Male"; value: "Male"}
                ListElement{key: "Female"; value: "Female"}
            }
        }

        TextField {
            id: textField4
             objectName: "dob"
            x: 271
            y: 97
            width: 145
            height: 24
            placeholderText: qsTr("Enter date of birth")
        }

        Label {
            id: label6
            x: 180
            y: 97
            width: 79
            height: 24
            text: qsTr("Date of birth:")
            font.pointSize: 10
        }
    }

    GroupBox {
        id: groupBox2
        x: 59
        y: 285
        width: 432
        height: 105
        title: qsTr("Address  Details")

        Label {
            id: label7
            x: 168
            y: 21
            width: 81
            height: 17
            text: qsTr("Place Of Birth")
            horizontalAlignment: Text.AlignHCenter
            font.pointSize: 10
        }

        Label {
            id: label8
            x: 0
            y: 18
            width: 96
            height: 19
            text: qsTr("Current Location")
            horizontalAlignment: Text.AlignHCenter
            font.pointSize: 10
            font.capitalization: Font.AllUppercase
        }

        Label {
            id: label9
            x: 312
            y: 21
            width: 72
            height: 16
            text: qsTr("Nationality")
            horizontalAlignment: Text.AlignHCenter
            font.pointSize: 10
        }

        TextField {
            id: textField5
             objectName: "location"
            x: 0
            y: 43
            width: 127
            height: 26
            placeholderText: qsTr("Enter current location")
        }

        TextField {
            id: textField6
             objectName: "placeofbirth"
            x: 145
            y: 44
            width: 127
            height: 25
            placeholderText: qsTr("Enter place of birth")
        }

        TextField {
            id: textField7
             objectName: "nationality"
            x: 285
            y: 44
            width: 127
            height: 25
            placeholderText: qsTr("Enter Nationality")
        }
    }

    GroupBox {
        id: groupBox3
        x: 9
        y: 409
        width: 525
        height: 138
        title: qsTr("Others")

        Label {
            id: label10
            x: 9
            y: 11
            width: 88
            height: 17
            text: qsTr("Phone Number")
            font.pointSize: 10
        }

        TextField {
            id: textField8
             objectName: "phone"
            x: 9
            y: 34
            width: 158
            height: 24
            placeholderText: qsTr("Enter Phone number")
        }

        Label {
            id: label11
            x: 241
            y: 15
            width: 92
            height: 13
            text: qsTr("Number of Crimes")
            font.pointSize: 10
        }

        TextField {
            id: textField9
             objectName: "crimes"
            x: 241
            y: 34
            width: 175
            height: 24
            placeholderText: qsTr("Enter number of crimes")
        }

        Label {
            objectName: "imgParth"
            id: label12
            x: 143
            y: 79
            width: 366
            height: 21
        }

        Button {
            id: button3
            x: 9
            y: 74
            text: qsTr("Choose Picture")
            onClicked: fileDialog.open()
    }

    Button {
        id: button1
         objectName: "btnAdd"
        x: 97
        y: 174
        width: 83
        height: 32
        text: qsTr("Add ")
       onClicked: simpleDialog.open()
    }

    Button {
        id: button2
         objectName: "btnCancel"
        x: 305
        y: 174
        width: 77
        height: 32
        text: qsTr("Cancel")
        onClicked: Qt.quit();
    }

//hapa chini tunadefine dialog box zetu

  Dialog{
        id: simpleDialog
        title:  "Success Message"
        Text {
            text: qsTr("Registration Successfuly")
        }
        onAccepted: this.close()
    }

  FileDialog{
         id: fileDialog
         selectMultiple: false
         selectFolder: false
         nameFilters: "*.jpg , *.png, *.PNG, *.jpeg"
         onAccepted:{
             selectMultiple ?
             console.log(this.fileUrls)
                       :
             label12.text = this.fileUrl

         }

     }


}
}
