import sys

from PyQt5.QtCore import QObject, QUrl, Qt
from PyQt5.QtWidgets import QApplication, QFileDialog, QWidget, QLabel
from PyQt5.QtQml import QQmlApplicationEngine

import face_recognition
import cv2
import sqlite3
import numpy as np
from base64 import b64decode
import random
import string
from threading import Timer
import logging
import traceback
import time

from imutils.face_utils import FaceAligner
from imutils.face_utils import rect_to_bb
import imutils
import dlib


def show(text):
    print(text)
    path = text
    fileUrl.setProperty("text", path)
    # image.setProperty("source", "file:///C:/Users/Apix/PycharmProjects/untitled1/img/hii.jpg")
    imgEditor(path[8:])


if __name__ == "__main__":
    app = QApplication(sys.argv)
    engine = QQmlApplicationEngine()
    ctx = engine.rootContext()
    ctx.setContextProperty("main", engine)
    engine.load('mat5.qml')
    win = engine.rootObjects()[0]
    win.textUpdated.connect(show)
    win.show()

    fullName = win.findChild(QObject, "fullName1")
    age = win.findChild(QObject, "age1")
    sex = win.findChild(QObject, "sex1")
    birth_date = win.findChild(QObject, "bDate1")
    birth_place = win.findChild(QObject, "pBirth1")
    nation = win.findChild(QObject, "nation1")
    address = win.findChild(QObject, "cAddress1")
    crime = win.findChild(QObject, "crime1")
    fileUrl = win.findChild(QObject, "label1")
    progressBar = win.findChild(QObject, "progressBar1")
    pic2 = win.findChild(QObject, "image2")
    status = win.findChild(QObject, "status")
    image = win.findChild(QObject, "image1")

    def cameraFct():

        conn = sqlite3.connect("Database/face.db")

        thecursor = conn.cursor()

        result = thecursor.execute("SELECT * FROM suspectsTable")

        data = result.fetchall()


        frame = 0

        def foo(event, x, y, flags, param):
            print('foo')
            # check which mouse button was pressed
            # e.g. play video on left mouse click
            if event == cv2.EVENT_LBUTTONDOWN:
                print('click')
                cv2.imwrite("img/use.jpg", frame)

            elif event == cv2.EVENT_RBUTTONDOWN:
                pass
                # do some other stuff on right click

        window_name = 'video'
        cv2.namedWindow(window_name)
        cv2.setMouseCallback(window_name, foo)

        video_capture = cv2.VideoCapture('http://192.168.1.2')
        #video_capture = cv2.VideoCapture('http://192.168.43.243:4747/mjpegfeed')

        # Initialize some variables
        face_locations = []
        face_encodings = []
        face_names = []
        process_this_frame = True

        while video_capture.isOpened():
            # Grab a single frame of video
            ret, frame = video_capture.read()

            # Resize frame of video to 1/4 size for faster face recognition processing
            small_frame = cv2.resize(frame, (0, 0), fx=0.25, fy=0.25)

            # Only process every other frame of video to save time
            if process_this_frame:
                # Find all the faces and face encodings in the current frame of video
                face_locations = face_recognition.face_locations(small_frame)
                face_encodings = face_recognition.face_encodings(small_frame, face_locations)

                face_names = []
                for face_encoding in face_encodings:
                    # See if the face is a match for the known face(s)
                    # match = face_recognition.compare_faces([said_face_encoding], face_encoding)
                    # face_distances = face_recognition.face_distance(known_encodings, face_encoding)
                    name = "Unknown"
                    for d in data:
                        print(d[1])
                        known_encodings = [np.fromstring(d[11], float)]
                        face_distance = face_recognition.face_distance(known_encodings, face_encoding)
                        if face_distance <= 0.3:
                            name = d[1] + " " + d[2]
                            break

                    # for face_distance in face_distances:
                    #     if face_distance < 0.3:
                    #         name = "Known"
                    #         break

                    face_names.append(name)

            process_this_frame = not process_this_frame

            # Display the results
            for (top, right, bottom, left), name in zip(face_locations, face_names):
                # Scale back up face locations since the frame we detected in was scaled to 1/4 size
                top *= 4
                right *= 4
                bottom *= 4
                left *= 4

                # Draw a box around the face
                cv2.rectangle(frame, (left, top), (right, bottom), (0, 0, 255), 2)

                # Draw a label with a name below the face
                cv2.rectangle(frame, (left, bottom - 35), (right, bottom), (0, 0, 255), cv2.FILLED)
                font = cv2.FONT_HERSHEY_DUPLEX
                cv2.putText(frame, name, (left + 6, bottom - 6), font, 1.0, (255, 255, 255), 1)

            # Display the resulting image
            cv2.imshow(window_name, frame)

            # Hit 'q' on the keyboard to quit!
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break

        conn.close()

        # Release handle to the webcam
        video_capture.release()
        cv2.destroyAllWindows()

    def photoFct():

        def foo(event, x, y, flags, param):
            # check which mouse button was pressed
            # e.g. play video on left mouse click
            if event == cv2.EVENT_LBUTTONDOWN:
                # chars = ''.join(random.sample(string.digits, 5))
                # path = "img/image_"+chars+".jpg"
                toa()

                path = "img/use.jpg"
                cv2.imwrite(path, frame)
                image.setProperty("source", "file:///C:/Users/Apix/PycharmProjects/untitled1/"+"Database/said1.jpg")
                image.setProperty("source", "file:///C:/Users/Apix/PycharmProjects/untitled1/"+path)
                fileUrl.setProperty("text", "file:///C:/Users/Apix/PycharmProjects/untitled1/"+path)

            elif event == cv2.EVENT_RBUTTONDOWN:
                pass
                # do some other stuff on right click

        window_name = 'video'
        cv2.namedWindow(window_name)
        cv2.setMouseCallback(window_name, foo)

        video_capture = cv2.VideoCapture('http://192.168.43.243:4747/mjpegfeed')
        # video_capture = cv2.VideoCapture(0)

        while video_capture.isOpened():

            ret, frame = video_capture.read()

            # Display the resulting image
            cv2.imshow(window_name, frame)

            # Hit 'q' on the keyboard to quit!
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break

        # Release handle to the webcam
        video_capture.release()
        cv2.destroyAllWindows()

    def searchFct():
        progressBar.setProperty("indeterminate", "true")
        timer = Timer(0.001, tafuta)
        timer.start()



        #
        # # Load a test image and get encondings for it
        #
        #
        # # See how far apart the test image is from the known faces
        # face_distances = face_recognition.face_distance(known_encodings, image_to_test_encoding)
        #
        # for i, face_distance in enumerate(face_distances):
        #     print("The test image has a distance of {:.2} from known image #{}".format(face_distance, i))
        #     print("- With a normal cutoff of 0.6, would the test image match the known image? {}".format(
        #         face_distance < 0.6))
        #     print("- With a very strict cutoff of 0.3, would the test image match the known image? {}".format(
        #         face_distance < 0.3))
        #     print()

    def toa():
        fullName.setProperty("text", "")
        sex.setProperty("text", "")
        birth_date.setProperty("text", "")
        birth_place.setProperty("text", "")
        nation.setProperty("text", "")
        address.setProperty("text", "")
        crime.setProperty("text", "")
        progressBar.setProperty("value", 0)
        progressBar.setProperty("indeterminate", "false")
        pic2.setProperty("source", "file:///C:/Users/User/PycharmProjects/face4/"+"Database/imagesPhoto.png")
        status.setProperty("text", "")

    def tafuta():
        try:
            path = fileUrl.property("text")
            print(path[8:])

            image_to_test = face_recognition.load_image_file(path[8:])
            image_to_test_encoding = face_recognition.face_encodings(image_to_test)[0]

            # known_fred_image = face_recognition.load_image_file("Tester/droid1.jpg")
            # known_fred1_image = face_recognition.load_image_file("Tester/droid2.jpg")
            # known_said_image = face_recognition.load_image_file("Tester/droid3.jpg")
            # known_said1_image = face_recognition.load_image_file("Tester/emily5.jpg")
            #
            # # Get the face encodings for the known images
            # fred_face_encoding = face_recognition.face_encodings(known_fred_image)[0]
            # fred1_face_encoding = face_recognition.face_encodings(known_fred1_image)[0]
            # said_face_encoding = face_recognition.face_encodings(known_said_image)[0]
            # said1_face_encoding = face_recognition.face_encodings(known_said1_image)[0]

            conn = sqlite3.connect("Database/face.db")

            thecursor = conn.cursor()

            result = thecursor.execute("SELECT * FROM suspectsTable")

            data = result.fetchall()

            found = 0

            for d in data:
                print(d[11])
                known_encodings = [np.fromstring(d[11], float)]
                face_distance = face_recognition.face_distance(known_encodings, image_to_test_encoding)
                if face_distance <= 0.3:
                    found = 1
                    progressBar.setProperty("indeterminate", "false")
                    progressBar.setProperty("value", 1)
                    fullName.setProperty("text", d[1] + " " + d[2])

                    sex.setProperty("text", d[4])
                    birth_date.setProperty("text", d[5])
                    birth_place.setProperty("text", d[7])
                    nation.setProperty("text", d[8])
                    address.setProperty("text", d[6])
                    crime.setProperty("text", d[10])
                    decode_str = b64decode(d[12])
                    # print(d[1] + " " + d[2])

                    import datetime
                    var = d[5]
                    before = datetime.datetime.strptime(var, '%d-%m-%Y')
                    now = datetime.datetime.now()

                    from datetime import datetime
                    from dateutil.relativedelta import relativedelta

                    difference_in_years = relativedelta(now, before).years

                    print(difference_in_years)
                    age.setProperty("text", difference_in_years)

                    fh = open("Database/imageToSave.jpg", "wb")
                    fh.write(decode_str)
                    fh.close()
                    pic2.setProperty("source", "file:///C:/Users/User/PycharmProjects/face4/"+"Database/imageToSave.jpg")
                    break

            if found == 0:
                progressBar.setProperty("indeterminate", "false")
                progressBar.setProperty("value", 1)
                status.setProperty("text", "MATCH NOT FOUND")

            conn.close()
        except Exception as e:
            logging.error(traceback.format_exc())

    def registPage():
        engine.load('registerPage.qml')

        win = engine.rootObjects()[0]
        win.show()


    def testFct():
        toa()
        window_name = 'video'
        cv2.namedWindow(window_name)

        video_capture = cv2.VideoCapture('http://192.168.43.71:4747/mjpegfeed')
        # video_capture = cv2.VideoCapture(0)

        face_locations = []
        process_this_frame = True

        while video_capture.isOpened():

            ret, frame = video_capture.read()

            # Resize frame of video to 1/4 size for faster face recognition processing
            small_frame = cv2.resize(frame, (0, 0), fx=0.25, fy=0.25)

            # Display the resulting image
            cv2.imshow(window_name, frame)

            if process_this_frame:
                # Find all the faces and face encodings in the current frame of video
                face_locations = face_recognition.face_locations(small_frame)

                for face_location in face_locations:
                    path = "img/use.jpg"
                    # timer = Timer(5, cv2.imwrite(path, frame))
                    # timer.start()
                    time.sleep(3)
                    cv2.imwrite(path, frame)
                    imgEditor("C:/Users/Apix/PycharmProjects/untitled1/" + path)
                    # fileUrl.setProperty("text", "file:///C:/Users/Apix/PycharmProjects/untitled1/" + path)
                    # image.setProperty("source", "file:///C:/Users/Apix/PycharmProjects/untitled1/" + "Database/said1.jpg")
                    # image.setProperty("source", "file:///C:/Users/Apix/PycharmProjects/untitled1/" + path)
                    break

            process_this_frame = not process_this_frame
            break

            # Hit 'q' on the keyboard to quit!
            # if cv2.waitKey(1) & 0xFF == ord('q'):
            #     break

        # Release handle to the webcam
        video_capture.release()
        cv2.destroyAllWindows()

    # def imgPicker():
    #     # toa()
    #     path = fileUrl.Property("text")
    #     imgEditor(path[8:])

    def imgEditor(path):
        detector = dlib.get_frontal_face_detector()
        predictor = dlib.shape_predictor("C:/Users/Apix/PycharmProjects/untitled1/shape_predictor_68_face_landmarks.dat")
        fa = FaceAligner(predictor, desiredFaceWidth=256)

        # load the input image, resize it, and convert it to grayscale
        # image = cv2.imread("C:/Users/Apix/PycharmProjects/untitled1/img/hey.jpg")
        image1 = cv2.imread(path)
        image1 = imutils.resize(image1, width=800)
        gray = cv2.cvtColor(image1, cv2.COLOR_BGR2GRAY)

        # show the original input image and detect faces in the grayscale
        # image
        rects = detector(gray, 2)

        # loop over the face detections
        for rect in rects:
            # extract the ROI of the *original* face, then align the face
            # using facial landmarks
            faceAligned = fa.align(image1, gray, rect)

            # display the output images
            cv2.imwrite("img/hii.jpg", faceAligned)
            image.setProperty("source", "file:///C:/Users/Apix/PycharmProjects/untitled1/" + "Database/said1.jpg")
            image.setProperty("source", "file:///C:/Users/Apix/PycharmProjects/untitled1/img/hii.jpg")
            fileUrl.setProperty("text", "file:///C:/Users/Apix/PycharmProjects/untitled1/img/hii.jpg")
            # cv2.waitKey(0)
        tafuta()

    regButton = win.findChild(QObject, "regButton")
    regButton.clicked.connect(lambda: registPage())

    importBtn = win.findChild(QObject, "importButton")
    importBtn.clicked.connect(lambda: toa())

    cameraBtn = win.findChild(QObject, "cameraButton")
    cameraBtn.clicked.connect(lambda: cameraFct())

    searchBtn = win.findChild(QObject, "searchButton")
    searchBtn.clicked.connect(lambda: searchFct())

    photoBtn = win.findChild(QObject, "photoButton")
    photoBtn.clicked.connect(lambda: testFct())

    sys.exit(app.exec_())

    # try:
    #
    # except Exception as e:
    #     logging.error(traceback.format_exc())